from tkinter import ttk
import tkinter as tk
from tkinter import messagebox,scrolledtext
from openpyxl import load_workbook
list1=[]
lista2=[]
lista3=[]
lista4=[]
class Aplication:
    def __init__(self):
        self.master = tk.Tk()
        self.master.title("BIENVENIDOS AL SISTEMA DE VENTAS")
        self.master.geometry("500x350")
        self.master.resizable(0, 0)
        self.note = ttk.Notebook(self.master)
        self.master.iconbitmap(r"E:\33.- Mis Programas en Python\Tkinter\74682.ico")
        self.note.pack(fill="both", expand="yes")
        self.Registro()
        self.Venta()
        self.Buscarproductos()
        self.master.mainloop()

    def Registro(self):
        self.codigos = tk.StringVar()
        self.description = tk.StringVar()
        self.cantidad = tk.StringVar()
        self.precio = tk.StringVar()
        self.pagina = ttk.Frame(self.note)
        self.note.add(self.pagina,text="REGISTRAR PRODUCTOS")
        self.label = tk.LabelFrame(self.pagina, text="REGISTRAR PRODUCTOS NUEVOS", background="light blue", borderwidth=1)
        self.label.pack(fill="both", expand="yes")
        self.name = tk.Label(self.label, text="CODIGO DEL PRODUCTO :", bg="light blue")
        self.name.place(x=2, y=5)
        self.entry = tk.Entry(self.label, textvariable=self.codigos)
        self.entry.focus()  # Para Ubicar el cursor en la entrada del entry parpadeando
        self.entry.place(x=180, y=5)
        self.name1 = tk.Label(self.label, text="DESCRIPCION DEL PRODUCTO :", bg="light blue")
        self.name1.place(x=2, y=30)
        self.entry1 = tk.Entry(self.label, textvariable=self.description)
        self.entry1.place(x=180, y=30)
        self.entry5 = tk.Entry(self.label, textvariable=self.cantidad)
        self.entry5.place(x=180, y=55)
        self.name2 = tk.Label(self.label, text="CANTIDAD :", bg="light blue")
        self.name2.place(x=2,y=55)
        self.name3 = tk.Label(self.label, text="PRECIO :", bg="light blue")
        self.name3.place(x=2, y=80)
        self.entry6 = tk.Entry(self.label, textvariable=self.precio)
        self.entry6.place(x=180, y=80)
        self.buscar= tk.Button(self.label, text="GUARDAR", relief="raised", command=lambda: self.Registrar(self.codigos, self.description, self.cantidad, self.precio))
        self.buscar.place(x=100,y=150)
        self.entry.bind('<Return>', lambda a: self.Registrar(self.codigos, self.description, self.cantidad, self.precio))   # para hacer funcionar con el boton enter del teclado
        self.entry1.bind('<Return>', lambda b: self.Registrar(self.codigos, self.description, self.cantidad, self.precio))  # para hacer funcionar con el boton enter del teclado
        self.entry5.bind('<Return>', lambda c: self.Registrar(self.codigos, self.description, self.cantidad, self.precio))  # para hacer funcionar con el boton enter del teclado
        self.entry6.bind('<Return>', lambda e: self.Registrar(self.codigos, self.description, self.cantidad, self.precio))  # para hacer funcionar con el boton enter del teclado


    def Venta(self):
        self.cod = tk.StringVar()
        self.descr = tk.StringVar()
        self.cantd = tk.StringVar()
        self.preci = tk.StringVar()
        self.total = tk.StringVar()
        self.pagina1 = ttk.Frame(self.note)
        self.note.add(self.pagina1,text="VENTAS")
        self.labelventa = tk.LabelFrame(self.pagina1, text="VENTA DE PRODUCTOS", background="light blue", borderwidth=1)
        self.labelventa.pack(fill="both", expand="yes")
        self.scrodes = tk.Listbox(self.labelventa,width=55,height=12,listvariable=self.descr)
        self.scrodes.place(x=60,y=65)
        self.scrocant = tk.Listbox(self.labelventa,width=8,height=12,listvariable=self.cantd)
        self.scrocant.place(x=5,y=65)
        self.scroprec = tk.Listbox(self.labelventa,width=13,height=12,listvariable=self.preci)
        self.scroprec.place(x=405,y=65)
        self.scrototal = tk.Listbox(self.labelventa,width=13,height=1,listvariable=self.total)
        self.scrototal.place(x=405,y=270)
        self.name4 = tk.Label(self.labelventa,text="CANTIDAD",bg="light blue")
        self.name4.place(x=1,y=40)
        self.name5 = tk.Label(self.labelventa,text="DESCRIPCION DEL ARTICULO",bg="light blue")
        self.name5.place(x=130,y=40)
        self.name6 = tk.Label(self.labelventa,text="PRECIO",bg="light blue")
        self.name6.place(x=415,y=40)
        self.name7 = tk.Label(self.labelventa,text="TOTAL",bg="light blue")
        self.name7.place(x=360,y=270)
        self.name8 = tk.Label(self.labelventa,text="CODIGO DEL PRODUCTO :",bg="light blue")
        self.name8.place(x=1,y=5)
        self.ventCodig = tk.Entry(self.labelventa,textvariable=self.cod)
        self.ventCodig.place(x=150,y=5)
        self.ventCodig.focus()
        self.ventCodig.bind('<Return>', lambda g: self.ventaProductos(self.cod))



    def Registrar(self, codigos, nombres, cant, precio):
        self.file=r"E:\33.- Mis Programas en Python\Tkinter\Libro1.xlsx"
        self.libro = load_workbook(self.file)
        self.ws =self.libro.active
        self.ws['A1'] = "CODIGO"
        self.ws['B1'] = "DESCRIPCION"
        self.ws['C1'] = "CANTIDAD"
        self.ws['D1'] = "PRECIO"
        self.ws['E1'] = "CODIGO DE BARRAS"
        self.libro.save(self.file)
        self.libro.close()
        self.value = codigos.get().upper()
        self.value1 = nombres.get().upper()
        self.value2 = cant.get()
        self.value3 = precio.get()

        if self.value != "" and self.value1 != "" and self.value2 != "" and self.value3 != "":
            self.entry.delete(0, "end")   # Sirve para limpiar la ventada de entrada y borrar lo que tenia
            self.entry1.delete(0, "end")  # Sirve para limpiar la ventada de entrada y borrar lo que tenia
            self.entry5.delete(0, "end")  # Sirve para limpiar la ventada de entrada y borrar lo que tenia
            self.entry6.delete(0, "end")  # Sirve para limpiar la ventada de entrada y borrar lo que tenia
            self.entry.focus()
            if self.value2.isdigit() and self.value3.isdigit():
                self.ws.append((self.value, self.value1, int(self.value2), float(self.value3)))
                self.libro.save(self.file)
                self.libro.close()
            else:
                messagebox.showerror(title="ERROR", message="Puso una Letra donde va cantidad")
        else:
            messagebox.showwarning(title="ALERT", message="Falta Descripción Código o Cantidad")

    def Buscarproductos(self):
        self.codigos1 = tk.StringVar()
        self.decr = tk.StringVar()
        self.canti = tk.IntVar()
        self.prec = tk.IntVar()
        self.pagina2 = ttk.Frame(self.note)
        self.note.add(self.pagina2,text="BUSCAR PRODUCTOS")
        self.labelbuscar = tk.LabelFrame(self.pagina2, text="BUSQUEDA DE PRODUCTOS", background="light blue", borderwidth=1)
        self.labelbuscar.pack(fill="both", expand="yes")
        self.nomb = tk.Label(self.labelbuscar, text="CODIGO A BUSCAR :", bg="light blue")
        self.nomb.place(x=2,y=5)
        self.desc = tk.Label(self.labelbuscar, text="DESCRICION :", bg="light blue")
        self.desc.place(x=2,y=30)
        self.cant = tk.Label(self.labelbuscar, text="CANTIDAD DISPONIBLE :", bg="light blue")
        self.cant.place(x=2,y=55)
        self.valor = tk.Label(self.labelbuscar, text="PRECIO :", bg="light blue")
        self.valor.place(x=2, y=80)
        self.entry2 = tk.Entry(self.labelbuscar, textvariable=self.codigos1)
        self.entry2.focus()
        self.entry2.place(x=180,y=5)
        self.entry3 = tk.Entry(self.labelbuscar, state="readonly", textvariable=self.decr)
        self.entry3.place(x=180,y=30)
        self.entry4 = tk.Entry(self.labelbuscar, state="readonly", textvariable=self.canti)
        self.entry4.place(x=180,y=55)
        self.entry7 =tk.Entry(self.labelbuscar, state="readonly", textvariable=self.prec)
        self.entry7.place(x=180, y=80)
        self.butonb = tk.Button(self.labelbuscar, text="BUSCAR", command=lambda: self.Productos(self.codigos1))
        self.butonb.place(x=100, y=150)
        self.entry2.bind('<Return>', lambda d: self.Productos(self.codigos1))


    def Productos(self,codi):
        self.file=r"E:\33.- Mis Programas en Python\Tkinter\Libro1.xlsx"
        self.libro = load_workbook(self.file)
        self.ws =self.libro.active
        valor = codi.get().upper()
        if valor:
            flag_Encontrado = 0
            for z in range(2, self.ws.max_row+1):
                num = self.ws['A{}'.format(z)]
                if num.value == valor:
                    des, cant, pre = self.ws['B{}:D{}'.format(z, z)][0]
                    self.decr.set(des.value)  # Se carga el valor en el input luego de ser rescatado desde la planilla
                    self.canti.set(cant.value)  # Se carga el valor en el input luego de ser rescatado desde la planilla
                    self.prec.set(pre.value)  # Se carga el valor en el input luego de ser rescatado desde la planilla
                    self.libro.close()
                    flag_Encontrado = 1
                    break # para detener el for una vez que encontro el producto a travez del código
            if flag_Encontrado == 0:
                messagebox.showerror(title="ERROR", message="Pieza No Encontrada")
                self.entry2.delete(0, 'end')
                self.decr.set("")
                self.canti.set("")
                self.prec.set("")
                self.entry2.focus()
                self.libro.close()
        else:
            messagebox.showerror(title="ERROR", message="Pieza No Encontrada")
            self.decr.set("")
            self.canti.set("")
            self.prec.set("")
            self.entry2.focus()
            self.libro.close()

    def ventaProductos(self,code):
        global a
        self.codig = code.get()
        self.file=r"E:\33.- Mis Programas en Python\Tkinter\Libro1.xlsx"
        self.libro = load_workbook(self.file)
        self.ws =self.libro.active
        if self.codig:
            flag_Encontrado1 = 0
            for y in range(2,self.ws.max_row+1):
                codigo = self.ws['E{}'.format(y)]
                if codigo.value == int(self.codig):
                    des, cant,prec = self.ws['B{}:D{}'.format(y,y)][0]
                    cantidad=cant.value
                    descripcion = des.value
                    precio = prec.value
                    if descripcion in list1:
                        x=list1.count(descripcion)
                        d=list1.index(descripcion)
                        lista2[d]=lista2[d]+1
                        lista3[d]=lista3[d]+precio
                        lista4.append(precio)
                        suma=0
                        for elementos in lista4:
                            suma= suma+elementos
                            self.total.set(suma)
                        self.cantd.set(lista2)
                        self.preci.set(lista3)
                        b=cantidad-1
                        self.ws['C{}'.format(y)]=b
                        self.libro.save(self.file)
                        self.ventCodig.delete(0, 'end')
                        if cantidad <= 1:
                            messagebox.showerror(title="ERROR",message="NO HAY MAS REPUESTOS")
                            flag_Encontrado1 = 1
                            self.libro.close()
                            break
                    else:
                        list1.append(descripcion)
                        a=list1.count(descripcion)
                        lista2.append(a)
                        lista3.append(precio)
                        lista4.append(precio)
                        suma=0
                        for elementos in lista4:
                            suma= suma+elementos
                            self.total.set(suma)
                        d=cantidad-1
                        self.ws['C{}'.format(y)]=d
                        self.libro.save(self.file)
                        self.ventCodig.delete(0, 'end')
                        self.descr.set(list1)
                        self.cantd.set(lista2)
                        self.preci.set(lista3)

                    flag_Encontrado1 = 1
                    break
            if flag_Encontrado1==0:
                messagebox.showwarning(title="WARNING",message="CODIGO NO ENCONTRADO")
                self.ventCodig.delete(0, 'end')
                self.libro.close()
        else:
            messagebox.showerror(title="ERROR",message="NO HAY CODIGO EN LA BARRA DE BUSQUEDA")






aplicacion = Aplication()